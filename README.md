
## docker 安装

- 设置 docker 中国镜像源
  ```sh
  curl https://set.wsl.fun/docker/cn
  ```

- 安装 docker
  ```sh
  # ubuntu | centos
  curl https://set.wsl.fun/docker/install
  ```